package calculadora;

import java.util.Scanner;

public class Calculadora {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		double res;
		
		System.out.println("Insira o valor inicial: ");
		double valorA = scan.nextDouble();
		
		System.out.println("Insira o operador: ");
		String operador = scan.next();
		
		System.out.println("Insira o valor final: ");
		double valorB = scan.nextDouble();
		
		switch(operador) {
		case "+": 
			res = valorA + valorB;
			System.out.println(valorA + " + " + valorB + " = "+ res); 
			break;
		case "-":
			res = valorA - valorB;
			System.out.println(valorA + " - " + valorB + " = "+ res); 
			break;
		case "*":
			res = valorA * valorB;
			System.out.println(valorA + " * " + valorB + " = "+ res); 
			break;
		case "/":
			res = valorA / valorB;
			System.out.println(valorA + " / " + valorB + " = "+ res); 
			break;
		case "%":
			res = valorA % valorB;
			System.out.println(valorA + " + " + valorB + " = "+ res); 
			break;
		default:
			System.out.println("Operador Invalido: ");
			
		
		
		}

	}

}
